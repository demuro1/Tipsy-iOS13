//
//  SummaryController.swift
//  Tipsy
//
//  Created by demuro1 on 1/15/21.
//  Copyright © 2021 The App Brewery. All rights reserved.
//

import UIKit


class SummaryController: UIViewController {
    @IBOutlet weak var individualTotalLbl: UILabel!
    @IBOutlet weak var splitDescriptionLbl: UILabel!
    
    var individualTotal: String?
    var splitCount: String?
    var tipPercent: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        individualTotalLbl.text = individualTotal
        splitDescriptionLbl.text = "Split between \(splitCount!) people, with \(tipPercent!) tip."
    }
    

    @IBAction func recalculateBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
