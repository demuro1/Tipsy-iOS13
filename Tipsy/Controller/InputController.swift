//
//  ViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class InputController: UIViewController {

    @IBOutlet weak var billAmountLabel: UITextField!
    @IBOutlet weak var zeroPercentBtn: UIButton!
    @IBOutlet weak var tenPercentBtn: UIButton!
    @IBOutlet weak var twentyPercentBtn: UIButton!
    @IBOutlet weak var splitQtyLbl: UILabel!
    
    var tipsyLogic = TipsyLogic()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func splitStepper(_ sender: UIStepper) {
        splitQtyLbl.text = String(format: "%.0f",sender.value)
        tipsyLogic.splitCount = splitQtyLbl.text!
    }
    
    
    @IBAction func tipSelection(_ sender: UIButton) {
        let tipPercent = tipsyLogic.highlightTip(percent: sender.currentTitle!)
        twentyPercentBtn.isSelected = tipPercent["20%"]!
        tenPercentBtn.isSelected = tipPercent["10%"]!
        zeroPercentBtn.isSelected = tipPercent["0%"]!
    }
    
    @IBAction func calculateBtn(_ sender: UIButton) {
        if billAmountLabel.text == nil || billAmountLabel.text == "" {
            tipsyLogic.totalAmount = "0.00"
        } else {
            tipsyLogic.totalAmount =  String(billAmountLabel.text!)
        }
                self.performSegue(withIdentifier: "goToSummary", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSummary" {
            let destinationVC = segue.destination as! SummaryController
            destinationVC.individualTotal = tipsyLogic.getIndividualTotal()
            destinationVC.splitCount = splitQtyLbl.text
            destinationVC.tipPercent = tipsyLogic.tipPercent
        }
    }
}

