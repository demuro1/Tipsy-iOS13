//
//  TipsyLogic.swift
//  Tipsy
//
//  Created by demuro1 on 1/17/21.
//  Copyright © 2021 The App Brewery. All rights reserved.
//

import Foundation

struct TipsyLogic{
    var totalAmount = "0.00"
    var splitCount = "2.0"
    var tipPercent = "10.0%"
    
    
    func getIndividualTotal() -> String {

        let amount = Float(totalAmount)!
        let percent = (Float(tipPercent.filter("0123456789.".contains))!)/100
        let count = Float(splitCount)!
        let individualTotal = ( amount * ( 1 + percent) ) / count
        return "$\(String(format:"%.2f",individualTotal))"
    }
    
    
    mutating func highlightTip(percent:String) -> [String: Bool] {
        tipPercent = percent
        var returnTips  = [String: Bool]()
        switch percent {
        case "20%":
            returnTips["20%"] = true
            returnTips["10%"] = false
            returnTips["0%"] = false
        case "10%":
            returnTips["20%"] = false
            returnTips["10%"] = true
            returnTips["0%"] = false
        case "0%":
            returnTips["20%"] = false
            returnTips["10%"] = false
            returnTips["0%"] = true
        default:
            returnTips["20%"] = false
            returnTips["10%"] = false
            returnTips["0%"] = true
        }
        return returnTips
    }
}
